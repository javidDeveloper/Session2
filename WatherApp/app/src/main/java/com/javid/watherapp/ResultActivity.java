package com.javid.watherapp;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    TextView yourName, yourField, yourNumber, yourAddress;
    Button back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        yourName = findViewById(R.id.your_name);
        yourField = findViewById(R.id.your_field);
        yourNumber = findViewById(R.id.your_number);
        yourAddress = findViewById(R.id.your_address);

        back = findViewById(R.id.back);

        String name = getIntent().getStringExtra("name");
        String field = getIntent().getStringExtra("field");
        String number = getIntent().getStringExtra("number");
        String address = getIntent().getStringExtra("address");
        Log.e("LOG", number);
        yourName.setText("نام و نام خانوادگی :" + " " + name);
        yourField.setText("رشته تحصیلی :" + " " + field);
        yourNumber.setText("شماره تماس :" + " " + number);
        yourAddress.setText("آدرس شما :" + " " + address);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ToastClass toastClass = new ToastClass();
        toastClass.reciveAlarm(ResultActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ToastClass toastClass = new ToastClass();
        toastClass.destoryAlarm(ResultActivity.this);
    }
}
