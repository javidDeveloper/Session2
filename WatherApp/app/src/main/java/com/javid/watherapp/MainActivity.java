package com.javid.watherapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText name, field, number, address;
    Button cancel, register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = findViewById(R.id.name);
        field = findViewById(R.id.field);
        number = findViewById(R.id.number);
        address = findViewById(R.id.address);
        cancel = findViewById(R.id.cancel);
        register = findViewById(R.id.register);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name.setText("");
                field.setText("");
                number.setText("");
                address.setText("");
                ToastClass toastClass = new ToastClass();
                toastClass.ClearAlarm(MainActivity.this);

            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ToastClass toastClass = new ToastClass();
                if (name.getText() == null) {
                    toastClass.EmptyAlarm(MainActivity.this);
                }
                if (field.getText() == null) {
                    toastClass.EmptyAlarm(MainActivity.this);
                }
                if (number.getText() == null) {
                    toastClass.EmptyAlarm(MainActivity.this);

                }
                if (address.getText() == null) {
                    toastClass.EmptyAlarm(MainActivity.this);
                } else {

                    Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                    intent.putExtra("name", name.getText().toString());
                    intent.putExtra("field", field.getText().toString());
                    intent.putExtra("number", number.getText().toString());
                    intent.putExtra("address", address.getText().toString());
                    Log.e("LOG", number.getText().toString());
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        ToastClass toastClass = new ToastClass();
        toastClass.stopAlarm(MainActivity.this);
    }



    @Override
    protected void onStart() {
        super.onStart();
        ToastClass toastClass = new ToastClass();
        toastClass.startAlarm(MainActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ToastClass toastClass = new ToastClass();
        toastClass.pauseAlarm(MainActivity.this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ToastClass toastClass = new ToastClass();
        toastClass.destoryAlarm(MainActivity.this);
    }
}

